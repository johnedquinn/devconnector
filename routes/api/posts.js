/*
 * File: posts.js
 * Author: John Ed Quinn
 * Description: NA
*/
const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');

const User = require('../../models/User');
const Profile = require('../../models/Profile');
const Post = require('../../models/Post');

// @route   POST api/posts
// @desc    Create a post
// @access  Private
router.post('/', [auth, [
    check('text', 'Text is required.').not().isEmpty()
]], async (req, res) => {
    // Check for syntax errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {
        // Get user
        const user = await User.findById(req.user.id).select('-password');

        // Create a variable to hold the post
        const newPost = new Post({
            text: req.body.text,
            name: user.name,
            avatar: user.avatar,
            user: req.user.id
        });

        const post = await newPost.save();

        return res.json(post);
    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }


});

// @route   GET api/posts
// @desc    Get all posts
// @access  Private
router.get('/', auth, async (req, res) => {
    try {
        // Get posts by recent first (oldest first is 1)
        const posts = await Post.find().sort({ date: -1 });

        return res.json(posts);
    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
});

// @route   GET api/posts/:post_id
// @desc    Get post by id
// @access  Private
router.get('/:id', auth, async (req, res) => {
    try {
        // Get post by ID
        const post = await Post.findById(req.params.id);
        if (!post) {
            return res.status(404).json({ msg: 'Post not found.' });
        }
        return res.json(post);
    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(404).json({ msg: 'Post not found.' });
        }
        return res.status(500).send('Server Error');
    }
});

// @route   DELETE api/posts/:post_id
// @desc    Delete a post by ID
// @access  Private
router.delete('/:post_id', auth, async (req, res) => {
    try {
        // Get post
        const post = await Post.findById(req.params.post_id);
        if (!post) {
            return res.status(404).json({ msg: 'Post not found.' });
        }

        // Make sure it is the user's post
        if (post.user.toString() != req.user.id) {
            return res.status(401).json({ msg: "User not authorized" });
        }

        await post.remove();

        return res.json({ msg: 'Post removed'});
    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(404).json({ msg: 'Post not found.' });
        }
        return res.status(500).send('Server Error');
    }
});

// @route   PUT api/posts/like/:post_id
// @desc    Like a post
// @access  Private
router.put('/like/:post_id', auth, async (req, res) => {
    try {
        // Get posts by recent first (oldest first is 1)
        const post = await Post.findById(req.params.post_id);
        if (!post) {
            return res.status(404).json({ msg: 'Post not found.' });
        }

        // Check if post has already been liked
        if (post.likes.filter(like => like.user.toString() == req.user.id).length > 0) {
            return res.status(400).json({ msg: 'Post already liked' });
        }

        // Add a like
        post.likes.unshift({ user: req.user.id });
        await post.save();

        return res.json(post.likes);

    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(404).json({ msg: 'Post not found.' });
        }
        return res.status(500).send('Server Error'); 
    }
});

// @route   PUT api/posts/unlike/:post_id
// @desc    Unlike a post
// @access  Private
router.put('/unlike/:post_id', auth, async (req, res) => {
    try {
        // Get posts by recent first (oldest first is 1)
        const post = await Post.findById(req.params.post_id);
        if (!post) {
            return res.status(404).json({ msg: 'Post not found.' });
        }

        // Check if post has already been liked
        if (post.likes.filter(like => like.user.toString() == req.user.id).length == 0) {
            return res.status(400).json({ msg: 'Post has not yet been liked.' });
        }

        // Get the remove index
        const removeIndex = post.likes.map(like => like.user.toString()).indexOf(req.user.id);
        if (removeIndex < 0) {
            return res.status(400).json({ msg: 'User not found in likes.' });
        }

        // Unlike post
        post.likes.splice(removeIndex, 1);
        await post.save();

        return res.json(post.likes);

    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(404).json({ msg: 'Post not found.' });
        }
        return res.status(500).send('Server Error'); 
    }
});

module.exports = router;