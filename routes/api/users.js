/*
 * File: users.js
 * Author: John Ed Quinn
 * Description: NA
*/
const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');

// Grab User schema
const User = require('../../models/User');

// @route   POST api/users
// @desc    Register user
// @access  Public
router.post('/', [
    check('name', 'Name is required.').not().isEmpty(),
    check('email', 'Please include a valid email.').isEmail(),
    check('password', 'Please enter a password with 6 or more characters').isLength({ min: 6 })
], async (req, res) => {
    // Check for errors in request
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    // Pull out variables from HTTP request
    const {name, email, password } = req.body;

    try {
        // See if user exists already
        let user = await User.findOne({ email });
        if (user) {
            res.status(400).json({ errors: [{ msg: 'User already exists' }] });
        }

        // Grab user avatar
        const avatar = gravatar.url(email, {
            s: '200',   // Size of 200
            r: 'pg',    // Only returns PG-rated avatars
            d: 'mm'     // Gives default image if non-existent
        });

        // Create instance of a user
        user = new User({
            name,
            email,
            avatar,
            password
        });

        // Encrypt password
        const salt = await bcrypt.genSalt(10);  // 10 recommended in documentation
        user.password = await bcrypt.hash(password, salt);

        // Save user to database
        await user.save();

        // Return JSON Web Token
        const payload = {
            user: {
                id: user.id,
            }
        }
        jwt.sign(payload, config.get('jwtSecret'), { expiresIn: 360000 }, (err, token) => {
            if (err) throw err;
            return res.json({ token });
        });

    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }

});

module.exports = router;