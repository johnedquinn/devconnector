# DevConnector

## Overview
This project creates a social media network for a target audience of software developers. It allows users to create profiles, author posts, post comments, and more. The framework is a MERN stack.

## Background
This is an implementation of a course I took on Udemy called [`MERN Stack Front To Back: Front Stack React, Redux & Node.js`](https://www.udemy.com/course/mern-stack-front-to-back/). The objective was to learn about how I can use MERN to potentially create a web application for startup ideas.

## Dependencies
Please see the [`package.json`](./package.json) file to see dependencies.

## Note
This project was based on the Udemy course, but there are several tweaks authored by myself.