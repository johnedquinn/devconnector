/*
 * File: auth.js
 * Author: John Ed Quinn
 * Description: NA
*/
const jwt = require('jsonwebtoken');
const config = require('config');

// Since is middleware function, has a next parameter
module.exports = function(req, res, next) {
    // Get token from the header --> When we send a request ...
    // ... to a protected route, we need to send token within a header
    const token = req.header('x-auth-token');

    // Check if no token
    if (!token) {
        return res.status(401).json({ msg: 'No token. Authorization denied.' });
    }

    // Verify token
    try {
        const decoded = jwt.verify(token, config.get('jwtSecret'));
        req.user = decoded.user;
        next();
    } catch (err) {
        res.status(401).json({ msg: 'Token is not valid.' });
    }
}